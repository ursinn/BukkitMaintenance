# BukkitMaintenance

[![Build Status Travis Master](https://img.shields.io/travis/com/ursinn/BukkitMaintenance/master?logo=travis&label=build%20master)](https://travis-ci.com/ursinn/BukkitMaintenance)
[![Build Status Travis Develop](https://img.shields.io/travis/com/ursinn/BukkitMaintenance/develop?logo=travis&label=build%20develop)](https://travis-ci.com/ursinn/BukkitMaintenance)
[![Build Status Jenkins Main](https://img.shields.io/jenkins/build?jobUrl=https%3A%2F%2Fci.filli-it.ch%2Fjob%2Fursinn%2Fjob%2FBukkitMaintenance%2Fjob%2Fmain%2F&label=build%20master&logo=jenkins)](https://ci.filli-it.ch/job/ursinn/job/BukkitMaintenance)
[![Build Status Jenkins Develop](https://img.shields.io/jenkins/build?jobUrl=https%3A%2F%2Fci.filli-it.ch%2Fjob%2Fursinn%2Fjob%2FBukkitMaintenance%2Fjob%2Fdevelop%2F&label=build%20develop&logo=jenkins)](https://ci.filli-it.ch/job/ursinn/job/BukkitMaintenance)

[![License: GPL v2](https://img.shields.io/github/license/ursinn/BukkitMaintenance)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)

Simple Maintenance Server for Minecraft
